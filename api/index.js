const express = require('express');
const oracledb = require('oracledb');
const bodyParser = require('body-parser');

let app = express();
app.use(bodyParser());

const allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    res.header(
      'Access-Control-Allow-Headers',
      'Content-Type, Authorization, access_token'
    )
  
    // intercept OPTIONS method
    if ('OPTIONS' === req.method) {
      res.send(200)
    } else {
      next()
    }
  };
  app.use(allowCrossDomain);

const connectionProperties = {
    user: "study2020",
    password: "R0uk!es2020",
    connectString: "sd10s0hthrgmtof.crf4gjs7a7c9.ap-northeast-1.rds.amazonaws.com/studydb",
    stmtCacheSize: 4,
    poolMin: 1,
    poolMax: 5
};

function doRelease(connection) {
    connection.release(function (err) {
        if (err) {
            console.error(err.message);
        }
    });
}

app.post('/query', function (req, res, next) {
    let connection;
    console.log(req.body);
    let sqltext = req.body.text;
    let sqlparams = req.body.params;

    if (typeof sqltext !== 'string'){
        sqltext="";
    }
    if (sqlparams === undefined){
        sqlparams = {};
    }

    oracledb.getConnection(connectionProperties)
    .then((conn) => {
        connection = conn;
        return conn.execute(sqltext, sqlparams, 
                { outFormat: oracledb.OBJECT });    
    })  
    .then((result) => {
        console.log(result);
        if (result.rows.length === 0) {
            res.send(500, {"err": "no data found"});
        } else {
            res.send({result:result.rows}); 
        }
        doRelease(connection);
        next();
    })
    .catch((err) => {
        console.error(err.message);

        if(connection != null){
            console.error(sqltext);
            doRelease(connection);
        }
        res.send(500, {"err": err.message});
        next();
    });
});

app.post('/execute', function (req, res, next) {
    let connection;
    console.log(req.body);
    let sqltext = req.body.text;
    let sqlparams = req.body.params;

    if (typeof sqltext !== 'string'){
        sqltext="";
    }
    if (sqlparams === undefined){
        sqlparams = {};
    }

    oracledb.getConnection(connectionProperties)
    .then((conn) => {
        connection = conn;
        console.log('do execute');
        console.log(sqlparams);
        return conn.execute(sqltext, sqlparams);    
    })
    .then((result)=>{
        console.log('execute ok');
        console.log(result);
        return connection.commit();//rowsAffected:
    })
    .then((result) => {
        doRelease(connection);
        res.send(200);
        next();
    })
    .catch((err) => {
        console.error(err.message);

        if(connection != null){
            doRelease(connection);
        }
        res.send(500, {"err": err.message});
        next();
    });
});
  
const server = app.listen(3001, function(){
    console.log("Node.js is listening to PORT:" + server.address().port);
});