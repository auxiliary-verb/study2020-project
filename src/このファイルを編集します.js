import {Query, Execute} from "./backend/Sql";

// ユーザー情報を更新します uid:ユーザーID uname:ユーザー名 gid:グループID
export const updateUserdata= async (uid, uname, gid)=> {
	console.log(uid.length);
	// 問 どちらの条件式が正しいでしょう
	/* ←この行をコメント化
	if (uid.length>10){
		return {'err': true,'msg': 'IDが長すぎます'};
	}//*/
	/* ←この行をコメント化
	if (uid.length>=10){
		return {'err': true,'msg': 'IDが長すぎます'};
	}//*/

	let result;
	//console.log(count);
	// 問 ユーザIDかぶりを回避しよう
	result = await Execute(`
	insert into t_user(user_id, GROUP_ID, user_name, LAST_UPDATE) 
	values (:userid, :groupid, :uname, sysdate) `, {userid: uid, uname: uname, groupid: gid});
	if (! result.err ){
		result.msg = "ユーザーを登録しました id:"+uid+"\tname:"+uname;
	}
	return result;
};

// DBから部署リストを取得します
export const  getGroupList = async ()=> {
	// 問 T_GROUPから部署名を取得するSQLを作成しよう
	let outComments = await Query(`
	select 'テスト'		as "GroupName"
		 , 0			as "GroupID"
      from dual`);
	console.log(outComments);
	return outComments;
};

// ユーザが存在するか確認します 
const checkUserExit = async (uid, gid) =>{
	let count = await Query(`
	select count(*) as "cnt"
	  from t_user where user_id = :userid 
	  and group_id = :groupid
	  `
	, {
		userid: uid, 
		groupid:gid,
	});
	return count[0].cnt;
};

// 表示する最大コメント数
const maxComment = 10;

// 表示情報
let infoColumns = ["ユーザーID", "Time"];

// DBからコメントを取得します
export const  getCommentList = async (index)=> {
	// ****ココにコメントを取得する処理を記述する****
	let outComments = await Query(`
	select 0 		as "No"
		 , 'test' 	as "comment"
		 , 1 		as "ユーザーID"
		 , sysdate 	as "Time"
      from dual`);
	console.log(outComments);
	// ****ココにコメントを取得する処理を記述する****
	return outComments;
};

// DBにコメントを送信します
export const  pushComment = async (uid, comment)=> {
	let error = true;
	let message = "";
	// ****ココにコメントの追加処理を記述する****

	if (comment === undefined || comment === ""){
	    error = true;
		message = "コメントが入力されていません";
	}else{
		/*
	    commentList.push({
	    	"No":getCommentCnt(),
	    	"ユーザーID":uid, 
	    	"comment":comment,
	    	"Time":sysdate(),
	    });//*/
	    error = false;
		message = "コメントしました";
	}
	// ****ココにコメントの追加処理を記述する****
    return {err: error, msg:message};
};

// DBにあるコメント数を取得します
export const getCommentCnt= async ()=>{
	// ****ココにコメント数を取得する処理を記述する****
	let contsql = await Query(`
	select 1 		as "count"
	  from dual`);
	let commentCnt = contsql[0].count;
	// ****ココにコメント数を取得する処理を記述する****
	return commentCnt;
};

export const  getInfoColumns = ()=>{
	return infoColumns;
};

const  sysdate = ()=>{
	let dt = new Date();
	let y = dt.getFullYear();
	let m = ("00" + (dt.getMonth()+1)).slice(-2);
	let d = ("00" + dt.getDate()).slice(-2);
	let h = ("00" + dt.getHours()).slice(-2);
	let mi = ("00" + dt.getMinutes()).slice(-2);
	let s = ("00" + dt.getSeconds()).slice(-2);
	let result = y + "/" + m + "/" + d + " " + h + ":" + mi + ":" + s;
	return result;
};

