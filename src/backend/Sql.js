// select文用
const url = 'http://localhost:3001'
export const Query = async (sql, params)=>{
    console.log(sql);
    const obj = {'text':sql, 'params': params};
    const method = "POST";
    const body = JSON.stringify(obj);
    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Origin': '*'
    };
    let out = await fetch(url + '/query', {
        mode: "cors",
        method:method,
        body: body,
        headers: headers
    })
    .then((response) => {
        return response.json(); 
    });
    //console.log(out.resulve);
    return out.result;
};

// update delete insert用
export const Execute=async (sql, params)=>{
    console.log(sql);
    const obj = {'text':sql, 'params': params};
    const method = "POST";
    const body = JSON.stringify(obj);
    const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Origin': '*'
    };
    let out = await fetch(url + '/execute', {
        mode: "cors",
        method:method,
        body: body,
        headers: headers
    })
    .then((response) => {
        if (response.ok){
            return {err: false}; 
        }else{
            return response.json()
            .then((value) => {
                return {err: true, msg: value.err};
            });
        }
    })
    .catch(error => {
        return {err: true, msg: error.message};
    });
    //console.log(out.resulve);
    return out;
};
