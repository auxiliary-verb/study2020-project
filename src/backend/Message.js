import React from 'react';

export default function Message(props) {
  return (
    <p className="Msg">
      {props.value}
    </p>
  );
}
