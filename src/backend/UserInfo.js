import React from 'react';
import {updateUserdata, getGroupList} from '../このファイルを編集します';

export default class UserInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {username:"", groupid:"", list:[]};
    }
    componentDidMount(){
        getGroupList().then(result => {
            console.log('list');
            console.log(result);
            this.setState({list:result});
        });
    }
    onClick = ()=>{
        const userid = this.props.uid;
        const {username, groupid} = this.state;
        updateUserdata(userid, username, groupid).then(result => {
            const {msg} = result;
            this.props.setMsg(msg);
        })
    };
    
    render() {
        const uid = this.props.uid;
        return (
        <div className="UserInfo">
            <UserId value={uid} onChange={(event)=>this.props.setUid(event.target.value)}/>
            <UserName value={this.state.username} onChange={(event)=>this.setState({username:event.target.value})}/>
            <GroupName value={this.state.groupid} list={this.state.list} onChange={(event)=>this.setState({groupid:event.target.value})}/>
            <button onClick={this.onClick}>
            登録
            </button>
        </div>
    );
    }
}

export function UserId(props) {
  return (
        <div className="UserId">
            <span>ユーザーID:</span>
            <input type="text" value={props.value} onChange={props.onChange}/>
        </div>
  );
}
export function UserName(props) {
  return (
        <div className="UserName">
            <span>ユーザー名:</span>
            <input type="text" value={props.value} onChange={props.onChange}/>
        </div>
  );
}
export function GroupName(props) {
    let list = [];
    list.push(
        <option key={"group-1"}
                value="">---選択して下さい---</option>);
    for (let listIndex in props.list){
        list.push(
        <option key={"group" + listIndex.toString()}
                value={props.list[listIndex].GroupID}>
            {props.list[listIndex].GroupName}
        </option>);
    }
    return (
          <div className="UserName">
              <span>部署名:</span>
              <select value={props.value} onChange={props.onChange}>{list}</select>
          </div>
    );
  }
  