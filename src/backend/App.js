import React from 'react';
import './App.css';
import Message from './Message';
import UserInfo from './UserInfo';
import CommentsArea from './CommentsArea';

export default class App extends React.Component {
  constructor(props){
    
    super(props);
    this.state = {uid: "", msg:"ココにメッセージを表示"};
  }
  
  setMsg=(message)=>{
    this.setState({msg:message});
  };

  render(props) {
    const {uid, msg} = this.state;
    return (
      <div className="App">
        <Message value={msg}/>
        <UserInfo uid={uid} setMsg={(message)=> this.setMsg(message)} setUid={(uid)=>this.setState({uid:uid})}/>
        <CommentsArea uid={uid} setMsg={(message)=> this.setMsg(message)}/>
      </div>
    );
  }
}
