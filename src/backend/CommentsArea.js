import React from 'react';
import {pushComment, getCommentList, getInfoColumns} from '../このファイルを編集します';

export default class CommentsArea extends React.Component {
    constructor(props) {
        super(props);
        const defaultindex = 0;
        this.state = {
            comments:null, 
	        infoList:getInfoColumns(),
            index:defaultindex,
        }
    }
    componentDidMount(){
        getCommentList(this.state.index).then(value => this.setState({comments:value}));
    }
    onSendComment = (comment)=>{
        pushComment(this.props.uid, comment).then(result => {
            const {err, msg} = result;
            this.props.setMsg(msg);
            if (!err){
                getCommentList(this.state.index)
                .then(value => this.setState({comments:value}));
            }
        })
    }
    
    onLoadComments = ()=>{
        getCommentList(this.state.index)
        .then(value => {
            this.setState({comments:value});
            this.props.setMsg("更新しました");
        } );
    }
    
    render() {
        return (
        <div className="CommentsArea">
            <CommentForm value={this.state.userid} onSend={this.onSendComment} onLoad={this.onLoadComments} />
            <CommentView comments={this.state.comments} infoList={this.state.infoList} />
        </div>
    );
    }
}

export class CommentForm  extends React.Component {
    constructor(props) {
        super(props);
        this.state = {comment:""};
    }
    
    setComment = (value)=> this.setState({comment:value});
    
    onSend=()=>{
        this.props.onSend(this.state.comment);
        this.setComment("");
    };
    
    onLoad=()=>{
        this.props.onLoad();
    };

    render(){
        const comment = this.state.comment;
        return (
            <p className="CommentForm">
                コメント:
                <input type="text" value={comment} onChange={(event)=>this.setComment(event.target.value)}/>
                <button className="SendButton" onClick={this.onSend}>送信</button>
                <button className="LoadButton" onClick={this.onLoad}>更新</button>
            </p>
        );
    }
}

export function CommentView(props) {
    let commentList=[];
    const comments = props.comments;
    const infoList = props.infoList;
    for(var commentsIndex in comments){
        const comment = comments[commentsIndex];
        let info = [];
        for(var infoIndex in infoList){
            const col = infoList[infoIndex];
            info.push(<li className="Info" key={col+commentsIndex.toString()}>
                {col+":"+comment[col]}
            </li>);
        }
        commentList.push(
            <div key={commentsIndex}>
                <ul className="InfoList">
                    {info}
                </ul>
                <p className="Comment">
                    {comment.comment}
                </p>
            </div>
            );
    }
    return (
        <div className="CommentView">
            {commentList}
        </div>
    );
}
